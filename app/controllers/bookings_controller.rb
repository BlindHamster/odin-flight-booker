class BookingsController < ApplicationController
  def new
    @flight = Flight.find(params[:booking][:flight_id])
    @booking = @flight.bookings.build
    @passengers = []
    params[:booking][:num_of_tickets].to_i.times do
      @passengers << Passenger.new
    end
  end

  def create
    params[:booking][:passenger_attributes].each do |key, value|
        @passenger = Passenger.new(user_params(value))
        @passenger.save
        @booking = @passenger.bookings.new(flight_id: params[:booking][:flight_id])
        @booking.save
        PassengerMailer.thank_you(@booking).deliver_later
    end
    redirect_to bookings_path
  end

  def index
    @bookings = Booking.includes(:passenger).includes(:flight).all
  end

  private

  def user_params(u_params)
    u_params.permit(:name, :email, :age, :sex)
  end
end
