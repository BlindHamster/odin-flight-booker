class FlightsController < ApplicationController

  def flights
    @flights = Flight.includes(:to_airport, :from_airport)
                      .all.order(flight_start: :asc)
    @dates = @flights.map { |f| f.flight_start.to_date }.uniq
    @from = @flights.map { |f| [f.from_airport.name, f.start_airport_id]}.uniq.sort
    @to = @flights.map { |f| [f.to_airport.name, f.end_airport_id]}.uniq.sort
    @filtered_flights = []

    unless params[:flight].nil?
      @flights.map { |f| @filtered_flights << f if matched?(f) }
    end
  end

  private
    def matched?(flight)
      params[:flight][:to_airport] == flight.end_airport_id.to_s &&
      params[:flight][:from_airport] == flight.start_airport_id.to_s &&
      params[:flight][:flight_start] == flight.flight_start.to_date.to_s
    end
end
