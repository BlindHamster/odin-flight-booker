class PassengerMailer < ApplicationMailer
  default from: "notifications@flight-booker.com"

  def thank_you(booking)
    @booking = booking
    @passenger = @booking.passenger
    @url = 'http://localhost:3000/bookings'
    mail(to: @passenger.email, subject: 'Your booking has been registered!')
  end
end
