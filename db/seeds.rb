# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Airport.create(name: "Barabash")

50.times do |i|
  name = Faker::Address.city
  Airport.create(name: name)
  puts "#{i+1}/50 airports created..."
end

100.times do |i|
  r1 = rand(1..50)
  start_airport = Airport.find(r1)
  begin
    r2 = rand(1..50)
    end_airport = Airport.find(r2)
  end while end_airport == start_airport
  flight_start = Time.now + rand(-100..100).hour
  flight_duration = rand(3..24)

  start_airport.departing_flights.create(end_airport_id: end_airport.id,
                                  flight_start: flight_start,
                                  flight_duration: flight_duration)
  puts "#{i+1}/100 flights created..."
end
