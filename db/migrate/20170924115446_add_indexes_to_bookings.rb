class AddIndexesToBookings < ActiveRecord::Migration[5.1]
  def change
    add_index :bookings, :passenger_id
    add_index :bookings, :flight_id
  end
end
