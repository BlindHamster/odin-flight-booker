class ChangeFlightDurationToInteger < ActiveRecord::Migration[5.1]
  def change
    change_column :flights, :flight_duration, :integer
  end
end
