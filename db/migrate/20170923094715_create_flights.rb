class CreateFlights < ActiveRecord::Migration[5.1]
  def change
    create_table :flights do |t|
      t.integer :start_airport_id, foreign_key: true
      t.integer :end_airport_id, foreign_key: true
      t.datetime :flight_start
      t.datetime :flight_duration
      t.timestamps
    end
    add_index :flights, [:start_airport_id, :end_airport_id, :flight_start], name: "start_end_time"
  end
end
