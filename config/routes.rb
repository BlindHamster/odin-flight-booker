Rails.application.routes.draw do

  root 'flights#flights'
  get '/flights', to: 'flights#flights'

  get '/booking', to: 'bookings#new'
  post '/booking', to: 'bookings#create'
  get '/bookings', to: 'bookings#index'
end
